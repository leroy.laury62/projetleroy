package com.example.projetleroy;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class QuestionActivity extends AppCompatActivity {

    Quiz quiz = null;
    TextView categoryTextView = null;

    Question currentQuestion = null;

    TextView questionTextView = null;

    TextView questionNumberTextView = null;

    RadioButton answer1 = null;
    RadioButton answer2 = null;
    RadioButton answer3 = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question);
        quiz = (Quiz) getIntent().getSerializableExtra("quiz");
        categoryTextView = findViewById(R.id.categoryTextView);
        categoryTextView.setText(quiz.getCategory());
        questionTextView = findViewById(R.id.question);
        questionNumberTextView = findViewById(R.id.questionNumber);
        answer1 = findViewById(R.id.answer1);
        answer2 = findViewById(R.id.answer2);
        answer3 = findViewById(R.id.answer3);
        loadQuestion();
    }

    public void loadQuestion() {
        RadioGroup radioGroup = findViewById(R.id.answerRadioGroup);
        radioGroup.clearCheck();
        Question question = quiz.nextQuestion();
        currentQuestion = question;
        questionTextView.setText(question.getQuestion());
        int questionNumber = quiz.getNumberOfQuestions() - quiz.getNumberOfRemainingQuestions();
        String questionNumberText = getString(R.string.question) + " " + questionNumber + " / " + quiz.getNumberOfQuestions();
        questionNumberTextView.setText(questionNumberText);
        answer1.setText(question.getAnswers()[0]);
        answer2.setText(question.getAnswers()[1]);
        answer3.setText(question.getAnswers()[2]);
        if (quiz.getNumberOfRemainingQuestions() == 0) {
            //change next button text to end string
            ((Button) findViewById(R.id.nextButton)).setText(R.string.end);
        }
    }

    public void cancel(View v) {
        finish();
    }

    public void next(View v) {
        //check if answer is correct
        RadioGroup radioGroup = findViewById(R.id.answerRadioGroup);
        int selectedId = radioGroup.getCheckedRadioButtonId();

        if (selectedId == -1) {
            //no answer selected
            return;
        }
        RadioButton radioButton = findViewById(selectedId);
        String answer = radioButton.getText().toString();
        quiz.answer(answer);

        if (quiz.getNumberOfRemainingQuestions() == 0) {
            //return quiz
            Intent intent = new Intent();
            intent.putExtra("quiz", quiz);
            setResult(RESULT_OK, intent);
            finish();
        } else {
            loadQuestion();
        }
    }
}