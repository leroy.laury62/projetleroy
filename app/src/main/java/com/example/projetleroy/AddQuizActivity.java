package com.example.projetleroy;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class AddQuizActivity extends AppCompatActivity {

    public enum CURRENT_STATE {
        CATEGORY,
        ADD_QUESTION,
        ADD_CORRECT_ANSWER,
        ADD_FALSE_ANSWER1,
        ADD_FALSE_ANSWER2,
    }

    private CURRENT_STATE currentState = CURRENT_STATE.CATEGORY;

    private final Quiz quiz = new Quiz();

    private Question question = new Question();

    private String[] answers = new String[3];

    private int cpt = 1;

    private EditText editText = null;
    private TextView textView = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_quiz);
        Toolbar toolbar = findViewById(R.id.toolBar);
        setSupportActionBar(toolbar);
        editText = findViewById(R.id.quizEditText);
        textView = findViewById(R.id.quizTextView);
    }

    public void cancel(View view) {
        finish();
    }

    public void next(View view) {
        switch (currentState) {

            //init phase, category of quiz
            case CATEGORY:
                if (editText.getText().toString().isEmpty()) {
                    Toast.makeText(this, R.string.add_category, Toast.LENGTH_SHORT).show();
                    return;
                }
                quiz.setCategory(editText.getText().toString());
                Resources res = getResources();
                String questionNumber = res.getQuantityString(R.plurals.enterQuestion, cpt, cpt);
                textView.setText(questionNumber);
                editText.setHint(R.string.yourQuestion);
                editText.setText("");

                currentState = CURRENT_STATE.ADD_QUESTION;
                break;
            case ADD_QUESTION:
                if (editText.getText().toString().isEmpty()) {
                    Toast.makeText(this, R.string.add_question, Toast.LENGTH_SHORT).show();
                    return;
                }
                question.setQuestion(editText.getText().toString());

                editText.setHint(R.string.correct_answer);
                editText.setText("");

                textView.setText(R.string.correct_answer);

                currentState = CURRENT_STATE.ADD_CORRECT_ANSWER;
                break;
            case ADD_CORRECT_ANSWER:
                if (editText.getText().toString().isEmpty()) {
                    Toast.makeText(this, R.string.add_answer, Toast.LENGTH_SHORT).show();
                    return;
                }
                answers[0] = editText.getText().toString();
                question.setCorrectAnswer(answers[0]);

                //change editTexts id
                editText.setHint(R.string.first_false_answer);
                editText.setText("");

                textView.setText(R.string.first_false_answer);
                currentState = CURRENT_STATE.ADD_FALSE_ANSWER1;
                break;
            case ADD_FALSE_ANSWER1:

                if (editText.getText().toString().isEmpty()) {
                    Toast.makeText(this, R.string.add_answer, Toast.LENGTH_SHORT).show();
                    return;
                }
                answers[1] = editText.getText().toString();

                //change editTexts
                editText.setHint(R.string.second_false_answer);
                editText.setText("");

                textView.setText(R.string.second_false_answer);

                //show save button between cancel and next
                findViewById(R.id.saveButton).setVisibility(View.VISIBLE);

                currentState = CURRENT_STATE.ADD_FALSE_ANSWER2;

                break;
            case ADD_FALSE_ANSWER2:

                if (editText.getText().toString().isEmpty()) {
                    Toast.makeText(this, R.string.add_answer, Toast.LENGTH_SHORT).show();
                    return;
                }

                answers[2] = editText.getText().toString();

                //shuffle answers
                for (int i = 0; i < answers.length; i++) {
                    int randomIndexToSwap = (int) (Math.random() * answers.length);
                    String temp = answers[randomIndexToSwap];
                    answers[randomIndexToSwap] = answers[i];
                    answers[i] = temp;
                }

                question.setAnswers(answers);
                quiz.addQuestion(question);
                question = new Question();
                answers = new String[3];
                cpt++;

                res = getResources();
                questionNumber = res.getQuantityString(R.plurals.enterQuestion, cpt, cpt);
                textView.setText(questionNumber);
                editText.setHint(R.string.yourQuestion);
                editText.setText("");

                //hide save button
                findViewById(R.id.saveButton).setVisibility(View.INVISIBLE);

                currentState = CURRENT_STATE.ADD_QUESTION;

                break;
        }
    }

    public void saveQuiz(View view) {

        if (editText.getText().toString().isEmpty()) {
            Toast.makeText(this, R.string.add_answer, Toast.LENGTH_SHORT).show();
            return;
        }

        answers[2] = editText.getText().toString();

        //shuffle answers
        for (int i = 0; i < answers.length; i++) {
            int randomIndexToSwap = (int) (Math.random() * answers.length);
            String temp = answers[randomIndexToSwap];
            answers[randomIndexToSwap] = answers[i];
            answers[i] = temp;
        }


        question.setAnswers(answers);
        quiz.addQuestion(question);


        //save to file
        String path = null;
        try {
            path = saveToFile();
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(this, R.string.error_saving, Toast.LENGTH_SHORT).show();
        }

        //return quiz to MainActivity
        getIntent().putExtra("quiz", quiz);
        getIntent().putExtra("path", path);
        setResult(RESULT_OK, getIntent());
        finish();
    }

    public String saveToFile() throws IOException {
        File file = new File(getApplicationInfo().dataDir + "/quiz/" + quiz.getCategory() + ".txt");
        if (!file.exists()) {
            file.createNewFile();
        }
        BufferedWriter writer = new BufferedWriter(new FileWriter(file));
        writer.write(quiz.getCategory());
        writer.newLine();
        for (Question question : quiz.getQuestions()) {
            writer.write(question.getQuestion());
            writer.newLine();
            for (String answer : question.getAnswers()) {
                writer.write("\t" + answer);
                if (answer.equals(question.getCorrectAnswer())) {
                    writer.write(" x");
                }
                writer.newLine();
            }
        }
        writer.close();
        Toast.makeText(this, R.string.quiz_saved, Toast.LENGTH_SHORT).show();


        return getApplicationInfo().dataDir + "/quiz/" + quiz.getCategory() + ".txt";
    }

}