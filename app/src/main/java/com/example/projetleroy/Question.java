package com.example.projetleroy;


import androidx.annotation.NonNull;

public class Question implements java.io.Serializable {

    private static final long serialVersionUID = 1L;

    private String question;
    private String[] answers;
    private String correctAnswer;

    public Question() {
        this.question = "";
        this.answers = new String[3];
        this.correctAnswer = "-1";
    }

    public String getQuestion() {
        return question;
    }

    public String[] getAnswers() {
        return answers;
    }

    public String getCorrectAnswer() {
        return correctAnswer;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public void setAnswers(String[] answers) {
        this.answers = answers;
    }

    public void setCorrectAnswer(String correctAnswer) {
        this.correctAnswer = correctAnswer;
    }

    public boolean isCorrect(String answer) {
        return answer.equals(correctAnswer);
    }

    @NonNull
    public String toString() {
        StringBuilder result = new StringBuilder(question + "\n");
        for (int i = 0; i < answers.length; i++) {
            result.append(i + 1).append(". ").append(answers[i]).append("\n");
        }
        return result.toString();
    }

    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, ClassNotFoundException {
        question = (String) in.readObject();
        answers = (String[]) in.readObject();
        correctAnswer = (String) in.readObject();
    }

    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
        out.writeObject(question);
        out.writeObject(answers);
        out.writeObject(correctAnswer);
    }

}
