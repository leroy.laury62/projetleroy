package com.example.projetleroy;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import java.util.Arrays;
import java.util.Locale;
import java.util.Objects;


public class SeeScoreActivity extends AppCompatActivity {

    Quiz[] quizList = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_see_score);
        Toolbar toolbar = findViewById(R.id.toolBar);
        setSupportActionBar(toolbar);

        ListView lv = findViewById(R.id.listView);
        Intent intent = getIntent();
        quizList = (Quiz[]) intent.getSerializableExtra("quizList");
        ScoreAdapter scoreAdapter = new ScoreAdapter(this, Arrays.asList(Objects.requireNonNull(quizList)));
        lv.setAdapter(scoreAdapter);

        TextView averageTextView = findViewById(R.id.averageTextView);
        float average = scoreAdapter.calculateAverage() * 20;  // Multiply by 20
        // show "average" message using R.string.average and append average value
        String averageString = getString(R.string.average) + " ";
        averageString += String.format(Locale.getDefault(), "%.2f / 20", average);
        averageTextView.setText(averageString);
    }


    public void Ok(View view) {
        finish();
    }


}