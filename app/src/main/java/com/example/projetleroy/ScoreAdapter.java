package com.example.projetleroy;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

public class ScoreAdapter extends BaseAdapter {
    private final Context context;
    private final List<Quiz> quizList;

    public ScoreAdapter(Context context, List<Quiz> quizList) {
        this.context = context;
        this.quizList = quizList;
    }

    @Override
    public int getCount() {
        return quizList.size();
    }

    @Override
    public Object getItem(int position) {
        return quizList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_quiz_score, null);
        }
        TextView categoryTextView = convertView.findViewById(R.id.categoryTextView);
        TextView scoreTextView = convertView.findViewById(R.id.scoreTextView);

        Quiz quiz = quizList.get(position);
        categoryTextView.setText(quiz.getCategory());
        String score = quiz.getScore() + " / " + quiz.getNumberOfQuestions();
        scoreTextView.setText(score);
        return convertView;
    }

    public float calculateAverage() {
        float average = 0;
        int numberOfQuestions = 0;
        for (Quiz quiz : quizList) {
            average += (float) quiz.getScore();
            numberOfQuestions += quiz.getNumberOfQuestions();
        }
        return average / numberOfQuestions;
    }


}
