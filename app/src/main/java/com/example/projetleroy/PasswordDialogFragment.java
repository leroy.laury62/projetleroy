package com.example.projetleroy;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.fragment.app.DialogFragment;

public class PasswordDialogFragment extends DialogFragment {

    public interface PasswordDialogListener {
        void onPasswordEntered(String password);
    }

    private EditText editTextPassword;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.popup_password, container, false);
        editTextPassword = view.findViewById(R.id.editTextPassword);
        Button btnSubmit = view.findViewById(R.id.btnSubmit);
        Button btnCancel = view.findViewById(R.id.btnCancel);

        btnSubmit.setOnClickListener(v -> {
            String enteredPassword = editTextPassword.getText().toString();
            PasswordDialogListener listener = (PasswordDialogListener) getActivity();
            if (listener != null) {
                listener.onPasswordEntered(enteredPassword);
            }
            dismiss();
        });

        btnCancel.setOnClickListener(v -> dismiss());

        return view;
    }
}
