package com.example.projetleroy;


import androidx.annotation.NonNull;

public class Quiz implements java.io.Serializable{


    private static final long serialVersionUID = 1L;

    private Question[] questions;
    private int score;

    private Question[] remainingQuestions;

    private String category;

    private Question currentQuestion;

    public Quiz() {
        this.questions = new Question[0];
        this.remainingQuestions = new Question[0];
        this.currentQuestion = null;
        score = 0;
    }

    public int getScore() {
        return score;
    }

    public Question nextQuestion() {
        //pick a random question from remaining questions
        int index = (int) (Math.random() * remainingQuestions.length);
        Question question = remainingQuestions[index];
        currentQuestion = question;

        //remove question from remaining questions
        Question[] newRemainingQuestions = new Question[remainingQuestions.length - 1];
        System.arraycopy(remainingQuestions, 0, newRemainingQuestions, 0, index);
        System.arraycopy(remainingQuestions, index + 1, newRemainingQuestions, index, remainingQuestions.length - index - 1);
        remainingQuestions = newRemainingQuestions;

        return question;
    }

    public void answer(String answer) {
        if (currentQuestion.isCorrect(answer)) {
            score++;
        }
    }

    public void addQuestion(Question question) {
        Question[] newQuestions = new Question[questions.length + 1];
        System.arraycopy(questions, 0, newQuestions, 0, questions.length);
        newQuestions[questions.length] = question;
        questions = newQuestions;

        Question[] newRemainingQuestions = new Question[remainingQuestions.length + 1];
        System.arraycopy(remainingQuestions, 0, newRemainingQuestions, 0, remainingQuestions.length);
        newRemainingQuestions[remainingQuestions.length] = question;
        remainingQuestions = newRemainingQuestions;
    }

    public int getNumberOfQuestions() {
        return questions.length;
    }

    public int getNumberOfRemainingQuestions() {
        return remainingQuestions.length;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public boolean isOver() {
        return remainingQuestions.length == 0;
    }

    public void reset() {
        score = 0;
        remainingQuestions = questions.clone();
    }

    public Question[] getQuestions() {
        return questions;
    }


    @NonNull
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append("quiz************************\n");
        result.append("Category: ").append(category).append("\n");
        for (Question question : questions) {
            result.append(question).append("\n");
        }
        result.append("Score: ").append(score).append("\n");
        result.append("************************************\n");
        return result.toString();
    }

    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, ClassNotFoundException {
        score = in.readInt();
        category = (String) in.readObject();
        currentQuestion = (Question) in.readObject();
        remainingQuestions = (Question[]) in.readObject();
        questions = (Question[]) in.readObject();
    }

    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
        System.out.println(this.category);
        out.writeInt(score);
        out.writeObject(category);
        out.writeObject(currentQuestion);
        out.writeObject(remainingQuestions);
        out.writeObject(questions);
    }

}
