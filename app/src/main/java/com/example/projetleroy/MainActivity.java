package com.example.projetleroy;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class MainActivity extends AppCompatActivity implements PasswordDialogFragment.PasswordDialogListener {

    protected Quiz[] quizList = new Quiz[0];
    private final ArrayList<String> arrayList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolBar);
        setSupportActionBar(toolbar);


        ListView lv = findViewById(R.id.listView);


        //load every qcm from phone
        String path = getApplicationInfo().dataDir + "/quiz";
        //create folder if it doesn't exist
        if (!new File(path).exists()) {
            new File(path).mkdir();
        }

        File[] files = new File(path).listFiles();


        if (files == null || files.length == 0) {
            Toast.makeText(this, R.string.no_files, Toast.LENGTH_SHORT).show();
        } else {

            //deserialize quizList if it exists
            boolean found = false;
            try {
                for (File file : files) {
                    if (!file.getName().endsWith(".ser")) {
                        continue;
                    }
                    String name = file.getName().substring(0, file.getName().length() - 4);
                    Quiz quiz = deserializeQuiz(getApplicationInfo().dataDir + "/quiz/" + name + ".ser");
                    quizList = Arrays.copyOf(quizList, quizList.length + 1);
                    quizList[quizList.length - 1] = quiz;
                    found = true;
                }
            } catch (Exception e) {
                quizList = new Quiz[0];
                Toast.makeText(this, R.string.error_loading, Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }


            //else try to load them from files
            if (!found) {
                for (File file : files) {
                    Quiz quiz = loadQuiz(file);
                    if (quiz == null) {
                        continue;
                    }
                    //add it to the list
                    quizList = Arrays.copyOf(quizList, quizList.length + 1);
                    quizList[quizList.length - 1] = quiz;
                }
            }
        }

        //for each quiz, add it to the list if not finished
        for (Quiz quiz : quizList) {
            if (!quiz.isOver()) {
                arrayList.add(quiz.getCategory());
            }
        }

        //display list
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, arrayList);
        lv.setAdapter(arrayAdapter);
        lv.setOnItemClickListener((parent, view, position, id) -> {
            Intent intent = new Intent(this, QuestionActivity.class);
            //get quiz
            Quiz quiz = null;
            for (Quiz q : quizList) {
                if (q.getCategory().equals(arrayList.get(position))) {
                    quiz = q;
                }
            }
            if (quiz == null) {
                return;
            }
            intent.putExtra("quiz", quiz);
            startQuizLauncher.launch(intent);
        });
    }

    ActivityResultLauncher<Intent> startQuizLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            result -> {
                if (result.getResultCode() == RESULT_OK) {
                    Intent data = result.getData();
                    if (data == null) {
                        return;
                    }
                    Quiz quiz = (Quiz) data.getSerializableExtra("quiz");
                    //update quiz
                    for (int i = 0; i < quizList.length; i++) {
                        if (quizList[i].getCategory().equals(quiz.getCategory())) {
                            quizList[i] = quiz;
                        }
                    }
                    arrayList.remove(quiz.getCategory());
                    ListView lv = findViewById(R.id.listView);
                    ArrayAdapter<String> arrayAdapter = (ArrayAdapter<String>) lv.getAdapter();
                    arrayAdapter.notifyDataSetChanged();
                }
            });

    ActivityResultLauncher<Intent> addQuizLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            result -> {
                if (result.getResultCode() == RESULT_OK) {
                    Intent data = result.getData();
                    if (data == null) {
                        return;
                    }
                    Quiz quiz = (Quiz) data.getSerializableExtra("quiz");
                    //add quiz to list
                    quizList = Arrays.copyOf(quizList, quizList.length + 1);
                    quizList[quizList.length - 1] = quiz;
                    arrayList.add(quiz.getCategory());
                    ListView lv = findViewById(R.id.listView);
                    ArrayAdapter<String> arrayAdapter = (ArrayAdapter<String>) lv.getAdapter();
                    arrayAdapter.notifyDataSetChanged();
                }
            });

    ActivityResultLauncher<Intent> mGetContent = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(),
            result -> {
                if (result.getResultCode() == RESULT_OK) {
                    Intent data = result.getData();
                    if (data == null) {
                        return;
                    }
                    Uri uri = data.getData();
                    if (uri == null) {
                        return;
                    }
                    InputStream inputStream = null;
                    try {
                        inputStream = getContentResolver().openInputStream(uri);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                    Quiz quiz = loadQuiz(inputStream);
                    if (quiz == null) {
                        return;
                    }
                    //add quiz to list
                    quizList = Arrays.copyOf(quizList, quizList.length + 1);
                    quizList[quizList.length - 1] = quiz;
                    arrayList.add(quiz.getCategory());
                    ListView lv = findViewById(R.id.listView);
                    ArrayAdapter<String> arrayAdapter = (ArrayAdapter<String>) lv.getAdapter();
                    arrayAdapter.notifyDataSetChanged();
                }
            });

    public Quiz loadQuiz(File file) {

        if (!file.getName().toLowerCase().endsWith(".txt")) {
            Toast.makeText(this, R.string.not_txt, Toast.LENGTH_SHORT).show();
            return null;
        }

        Quiz quiz = new Quiz();

        try (Scanner content = new Scanner(file)) {
            // Read and set the category
            if (content.hasNextLine()) {
                quiz.setCategory(content.nextLine());
            } else {
                Toast.makeText(this, R.string.empty_file, Toast.LENGTH_SHORT).show();
                return null; // Return null if no category is found
            }

            Question question = null;
            List<String> answersList = new ArrayList<>();

            // Process each line
            try {
                while (content.hasNextLine()) {
                    String line = content.nextLine();

                    if (line.trim().isEmpty()) {
                        continue; // Skip empty lines
                    }

                    if (Character.isLetter(line.charAt(0)) || Character.isDigit(line.charAt(0))) {
                        // If the line starts with a letter or number, it's a new question
                        if (question != null) {
                            // Add the previous question to the quiz
                            question.setAnswers(answersList.toArray(new String[0]));
                            quiz.addQuestion(question);
                        }

                        // Create a new question
                        question = new Question();
                        question.setQuestion(line);
                        answersList.clear();
                    } else if (line.startsWith("\t") || line.startsWith(" ")) {
                        // If the line starts with tabulation or space, it's an answer
                        String answer = line.trim();

                        if (answer.endsWith("x")) {
                            // If it's the correct answer
                            question.setCorrectAnswer(answer.substring(0, answer.length() - 1));
                            answersList.add(answer.substring(0, answer.length() - 1));
                        } else {
                            answersList.add(answer);
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(this, R.string.error_loading, Toast.LENGTH_SHORT).show();
                return null;
            }

            // Add the last question to the quiz if it exists
            if (question != null) {
                question.setAnswers(answersList.toArray(new String[0]));
                quiz.addQuestion(question);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Toast.makeText(this, R.string.error_loading, Toast.LENGTH_SHORT).show();
            return null;
        }

        return quiz;
    }

    public Quiz loadQuiz(InputStream inputStream) {

        Quiz quiz = new Quiz();

        try (Scanner content = new Scanner(inputStream)) {
            // Read and set the category
            if (content.hasNextLine()) {
                quiz.setCategory(content.nextLine());
                //check if category already exists
                for (Quiz q : quizList) {
                    if (q.getCategory().equals(quiz.getCategory())) {
                        Toast.makeText(this, R.string.quiz_already_exist, Toast.LENGTH_SHORT).show();
                        return null;
                    }
                }
            } else {
                Toast.makeText(this, R.string.empty_file, Toast.LENGTH_SHORT).show();
                return null; // Return empty quiz if no category is found
            }

            Question question = null;
            List<String> answersList = new ArrayList<>();

            // Process each line
            try {
                while (content.hasNextLine()) {
                    String line = content.nextLine();

                    if (line.trim().isEmpty()) {
                        continue; // Skip empty lines
                    }

                    if (Character.isLetter(line.charAt(0)) || Character.isDigit(line.charAt(0))) {
                        // If the line starts with a letter or number, it's a new question
                        if (question != null) {
                            // Add the previous question to the quiz
                            question.setAnswers(answersList.toArray(new String[0]));
                            quiz.addQuestion(question);
                        }

                        // Create a new question
                        question = new Question();
                        question.setQuestion(line);
                        answersList.clear();
                    } else if (line.startsWith("\t") || line.startsWith(" ")) {
                        // If the line starts with tabulation or space, it's an answer
                        String answer = line.trim();

                        if (answer.endsWith("x")) {
                            // If it's the correct answer
                            question.setCorrectAnswer(answer.substring(0, answer.length() - 1));
                            answersList.add(answer.substring(0, answer.length() - 1));
                        } else {
                            answersList.add(answer);
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(this, R.string.error_loading, Toast.LENGTH_SHORT).show();
                return null;
            }

            // Add the last question to the quiz if it exists
            if (question != null) {
                question.setAnswers(answersList.toArray(new String[0]));
                quiz.addQuestion(question);
            }
        }

        return quiz;
    }

    public void resetScore(MenuItem item) {
        for (Quiz quiz : quizList) {
            if (quiz.isOver()) {
                arrayList.add(quiz.getCategory());
            }
            quiz.reset();
        }
        ListView lv = findViewById(R.id.listView);
        ArrayAdapter<String> arrayAdapter = (ArrayAdapter<String>) lv.getAdapter();
        arrayAdapter.notifyDataSetChanged();
        Toast.makeText(this, R.string.score_reset, Toast.LENGTH_SHORT).show();
    }

    public void quit(MenuItem item) {
        //serialize quizList
        for (Quiz quiz : quizList) {
            try {
                serializeQuiz(quiz);
            } catch (IOException e) {
                e.printStackTrace();
                Toast.makeText(this, R.string.error_saving, Toast.LENGTH_SHORT).show();
            }
        }
        finish();
    }

    public void serializeQuiz(Quiz quiz) throws IOException {
        FileOutputStream fileOut = new FileOutputStream(getApplicationInfo().dataDir + "/quiz/" + quiz.getCategory() + ".ser");
        ObjectOutputStream fluxOut = new ObjectOutputStream(fileOut);
        fluxOut.writeObject(quiz);
        fluxOut.close();
        fileOut.close();
    }

    public Quiz deserializeQuiz(String path) throws IOException {

        FileInputStream fileIn = new FileInputStream(path);
        ObjectInputStream fluxIn = new ObjectInputStream(fileIn);
        Quiz quiz = null;
        try {
            quiz = (Quiz) fluxIn.readObject();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        fluxIn.close();
        fileIn.close();
        return quiz;
    }

    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.resetScore) {
            resetScore(item);
            return true;
        }
        if (item.getItemId() == R.id.quit) {
            quit(item);
            return true;
        }
        if (item.getItemId() == R.id.addQuiz) {
            showPasswordDialog();
            return true;
        }
        if (item.getItemId() == R.id.seeScore) {
            Intent intent = new Intent(this, SeeScoreActivity.class);
            intent.putExtra("quizList", quizList);
            startActivity(intent);
            return true;
        }
        if (item.getItemId() == R.id.importFromFile) {
            Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.setType("text/plain");
            mGetContent.launch(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showPasswordDialog() {
        PasswordDialogFragment passwordDialogFragment = new PasswordDialogFragment();
        passwordDialogFragment.show(getSupportFragmentManager(), "PasswordDialogFragment");
    }

    @Override
    public void onPasswordEntered(String password) {
        if (password.equals("MDP")) {
            Intent intent = new Intent(this, AddQuizActivity.class);
            addQuizLauncher.launch(intent);
        } else {
            Toast.makeText(this, "Mot de passe incorrect", Toast.LENGTH_SHORT).show();
        }
    }


}

